//
//  TapViewViewController.swift
//  gestures GR2
//
//  Created by Sebas on 22/1/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit

class TapViewViewController: UIViewController {

    
    @IBOutlet weak var customView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func tapGestureAction(_ sender: Any) {
        
        customView.backgroundColor = .red
        
    }
    
    @IBAction func swipeUpAction(_ sender: Any) {
        
        customView.backgroundColor = UIColor(red: 24.0/255, green: 69.0/255, blue: 98.0/255, alpha: 1)
        
    }
    
    
}
