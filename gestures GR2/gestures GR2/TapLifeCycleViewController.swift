//
//  TapLifeCycleViewController.swift
//  gestures GR2
//
//  Created by Sebas on 22/1/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit

class TapLifeCycleViewController: UIViewController {

    @IBOutlet weak var tapsLabel: UILabel!
    
    @IBOutlet weak var touchesLabel: UILabel!
    
    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
//        self.next?.touchesBegan(touches, with: event)

        let touchesCount = touches.count
     
        let touch = touches.first!
        let tapCount = touch.tapCount
        
        if customView.frame.contains(touch.location(in: self.view)) {
            
            print("Alerta")
            
        } else {
            
            touchesLabel.text = "\(touchesCount)"
            tapsLabel.text = "\(tapCount)"
        }
 
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        let location = touch?.location(in: self.view)
        
        let x = location?.x
        let y = location?.y
        
        print("x : \(x ?? 0) y: \(y ?? 0)")
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
//        tapsLabel.text = "0"
//        touchesLabel.text = "0"
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("canceled")
    }

}






